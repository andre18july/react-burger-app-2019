import React, {Component} from 'react';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import { Route, Redirect } from 'react-router-dom';
import ContactData from './ContactData/ContactData';
import { connect } from 'react-redux';


class Checkout extends Component{


    checkoutCancelledHandler = () => {
        this.props.history.goBack();
    }


    checkoutContinuedHandler = () => {
        //this.props.history.replace(this.props.match.url + "/contact-data");

        this.props.history.replace("/checkout/contact-data");
    }

    render(){

        //console.log(this.state);

        let summary = <Redirect to ="/" />

        

        if(this.props.ingredients){

            const purchasedRedirect = this.props.purchased ? <Redirect to='/'/> : null;

            summary = (

                <div>
                
                    {purchasedRedirect}
                    <CheckoutSummary 
                        checkoutCancelled={this.checkoutCancelledHandler}
                        checkoutContinued={this.checkoutContinuedHandler}
                        ingredients={this.props.ingredients}
                        totalPrice={this.props.totalPrice}/>

                    <Route 
                        path={this.props.match.path + "/contact-data"} 
                        component={ContactData}
                    />


                </div>
            );
        }

        return summary;

    }
}


const mapStateToProps = state => {

    return{
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        purchased: state.order.purchased
    }
}




export default connect(mapStateToProps)(Checkout);