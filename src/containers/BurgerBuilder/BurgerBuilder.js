import React, { Component } from 'react';
import { connect } from 'react-redux';

import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-orders';

import * as actions from '../../store/actions/index';




export class BurgerBuilder extends Component{

    state = {

        //purchasable: false,
        ordered: false,

    }


    /*
    componentDidMount() {
        axios.get('https://react-my-burger-92546.firebaseio.com/ingredients.json')
        .then((response) => {
            //console.log(response.data);
            this.setState({
                ingredients: response.data
            });
        })
        .catch((error) => {
            this.setState({
                error: true
            })
        })
    }
    */
    
    

    
    async componentDidMount() {
        this.props.onInitIngredients();
    }
    
    
 
    updatePurchaseState = (ingredients) => {

        //console.log(this);

        let purchaseState = false;
        
        for(const key in ingredients){
            if(ingredients[key] > 0){
                return purchaseState = true;
            }
        }

        return purchaseState;

    }

    orderHandler = () =>  {
        if(this.props.isAuth){
            this.setState({ordered: true})
        }else{
            this.props.onSetAuthRedirectPath("/checkout");
            this.props.history.push("/auth");
        }
    }

    orderCancelled = () => {
        this.setState({ordered: false})
    }

    purchaseContinuedHandler = async () => {   
        this.props.onInitPurchased();    
        this.props.history.push('/checkout');
    }



    render(){
        //console.log('Inside BurgerBuilder --- ' + this.state.ordered);


        let disabledIngredients = null;

        if(this.props.ingredients){
            disabledIngredients = { ...this.props.ingredients };
            for(const key in disabledIngredients){
                disabledIngredients[key] = disabledIngredients[key] <= 0 ? true : false;
            }
        }

        //console.log(disabledIngredients);
   
        //console.log("ESTADO PUR " + this.state.purchasable);


        let orderSummary = null;


        let burger = this.props.error ? <p>Ingredients can't be loaded!</p> : <Spinner />;
        

        if(this.props.ingredients){

            burger = (
    
                <>
            
                    <Burger ingredients={this.props.ingredients}/>
                        
                    <BuildControls
                        isAuthenticated={this.props.isAuth}
                        addIngredient={this.props.onIngredientAdded} 
                        removeIngredient={this.props.onIngredientRemoved}
                        disabled={disabledIngredients}     
                        price={this.props.totalPrice}
                        purchasable={this.updatePurchaseState(this.props.ingredients)}
                        order={this.orderHandler}

                    />
    
                </>
            );

            orderSummary = (
                <OrderSummary 
                    ingredients={this.props.ingredients} 
                    total={this.props.totalPrice}
                    purchaseCancelled={this.orderCancelled}
                    purchaseContinued={this.purchaseContinuedHandler}

                />
            );

        }

        
        return(
            <>
                <Modal show={this.state.ordered} modalClosed={this.orderCancelled}>
                    {orderSummary}
                </Modal>
                {burger}
            </>
        )
    }

}


const mapStateToProps = state => {

    return{
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        isAuth: state.auth.token !== null
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdded: (ingredientName) => dispatch(actions.addIngredient(ingredientName)),
        onIngredientRemoved: (ingredientName) => dispatch(actions.removeIngredient(ingredientName)),
        onInitIngredients: () => dispatch(actions.initIngredients()),
        onInitPurchased: () => dispatch(actions.purchaseInit()),
        onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));