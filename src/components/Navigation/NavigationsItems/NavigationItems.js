import React from 'react';
import styles from './NavigationItems.module.css';
import NativationItem from './NavigationItem/NavigationItem';


const navigationItems = (props) => (

    <ul className={styles.NavigationItems}>
        <NativationItem link="/" exact>Burger Builder</NativationItem>
        {props.isAuthenticated ? <NativationItem link="/orders">Orders</NativationItem> : null}
        {props.isAuthenticated ? 
            <NativationItem link="/logout">Logout</NativationItem> : 
            <NativationItem link="/auth">Authenticate</NativationItem> 
        }

    </ul>
)

export default navigationItems;