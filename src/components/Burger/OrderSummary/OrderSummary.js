import React , { Component } from 'react';
import Button from '../../UI/Button/Button';


class OrderSummary extends Component{

    componentWillUpdate(){
        //console.log('[OrderSummary] Will UPdate');
    }

    render(){

        const ingredientsSummary = Object.keys(this.props.ingredients).map((key) => {
            return( 
                <li key={key}>
                    <span style={{textTransform: 'capitalize'}}>{key}</span>: {this.props.ingredients[key]}
                </li>
            );
        });

        return(
            <>
                <h3>Your Order</h3>
                <p>A delicious burger with the following ingredients:</p>
                <ul>
                    {ingredientsSummary}
                </ul>
                <p><strong>Total Price: </strong>{this.props.total.toFixed(2)} {'\u20AC'}</p>
                <p>Continue to Checkout?</p>
                <Button btnType="Danger" clicked={this.props.purchaseCancelled}>CANCEL</Button>
                <Button btnType="Success" clicked={this.props.purchaseContinued}>CONTINUE</Button>
            </>
        );

    }


}

export default OrderSummary;